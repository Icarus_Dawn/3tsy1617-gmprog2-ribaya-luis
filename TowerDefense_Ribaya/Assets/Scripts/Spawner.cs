﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject EnemyPrefab;
    public Transform SpawnPoint;
    public float Timer;
    public float SpawnIncrement;
    private int WaveNumber;
    private int EnemyCount;
    public int MaxEnemyCount;

    void Start()
    {
        EnemyCount = 0;
    }

    void Update()
    {
        Timer -= Time.deltaTime;

        if (Timer <= 0 && EnemyCount < MaxEnemyCount)
        {
            Instantiate(EnemyPrefab, SpawnPoint.position, Quaternion.identity);
            EnemyCount++;
            Timer = SpawnIncrement;               
        }
    }
    //public Transform enemyPrefab;
    //public Transform spawnpoint;
    //public float waveinterval = 5f;
    //private float countdown = 3f;
    //private int enemynum = 1;

    //void Update()
    //{
    //    if (countdown <= 0)
    //    {
    //        WaveSpawner();
    //        countdown = waveinterval;
    //    }

    //    countdown -= Time.deltaTime; 
    //}

    //void WaveSpawner()
    //{
    //    Debug.Log("Wave Spawned!");
    //    for (int i = 0; i < enemynum; i++)
    //    {
    //        SpawnEnemy();
    //    }
    //    enemynum++;
    //}

    //void SpawnEnemy()
    //{
    //    Instantiate(enemyPrefab, spawnpoint.position, spawnpoint.rotation);

    //}
}
