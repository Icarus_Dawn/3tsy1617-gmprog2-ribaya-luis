﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemymovement : MonoBehaviour {

	public Transform EndGoal;

	void Start () 
	{
		NavMeshAgent agent = GetComponent<NavMeshAgent>();
		agent.destination = EndGoal.position;
	}

}
