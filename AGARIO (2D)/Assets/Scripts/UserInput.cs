﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserInput : MonoBehaviour
{
    public Text Pause;

	// Use this for initialization
	void Start ()
    {
       // Pause = GetComponent<Text>();
        Pause.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
                Pause.gameObject.SetActive(true);
            }
            else
            {
                Time.timeScale = 1;
                Pause.gameObject.SetActive(false);
            }
            
        }	
	}
}
