﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public Vector3 PlayerPos;
    public Transform Player;
    // Update is called once per frame
    void Update()
    {
        transform.position = Player.position + PlayerPos;
    }
}
