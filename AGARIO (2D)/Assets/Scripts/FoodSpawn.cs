﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodSpawn : MonoBehaviour
{
    public GameObject prefabfood;
    public float SpawnSpeed;
    public int spawnlimit;
    public int spawncount;
    void Start()
    {
        InvokeRepeating("Spawner", 0, SpawnSpeed);
        //for (int i = 0; i < 100; i++)
        //{
        //    Spawner();
        //    spawncount++;
        //}   
    }

    void Update()
    {
       
    }

    void Spawner()
    {
        int x = Random.Range(0, Camera.main.pixelWidth);
        int y = Random.Range(0, Camera.main.pixelHeight);

        Vector3 Target = Camera.main.ScreenToWorldPoint(new Vector3 (x, y, 0));
        Target.z = 0;

        Instantiate(prefabfood, Target, Quaternion.identity);
    }
}
