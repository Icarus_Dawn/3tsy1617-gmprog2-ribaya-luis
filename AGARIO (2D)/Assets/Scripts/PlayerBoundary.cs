﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBoundary : MonoBehaviour
{   
    public float minX;
    public float maxX;
    public float minY;
    public float maxY;
	// Update is called once per frame
	void Update ()
    {
       this.transform.position = new Vector2( Mathf.Clamp(transform.position.x, minX, maxX),
        Mathf.Clamp(transform.position.y, minY, maxY));
	}
}
