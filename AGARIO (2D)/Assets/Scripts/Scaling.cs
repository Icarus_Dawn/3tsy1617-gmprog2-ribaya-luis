﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scaling : MonoBehaviour
{
    public float sizeSpeed = 2;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            transform.localScale += new Vector3(0.1f, 0.1f, 0);
            Camera.main.orthographicSize += 0.1f;
        }

        if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            transform.localScale -= new Vector3(0.1f, 0.1f, 0);
            Camera.main.orthographicSize -= 0.1f;
        }
	}
}
