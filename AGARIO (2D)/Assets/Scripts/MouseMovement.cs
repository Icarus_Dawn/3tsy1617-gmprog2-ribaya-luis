﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMovement : MonoBehaviour
{
    
    public float MoveSpeed;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 Target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Target.z = transform.position.z;

        transform.position = Vector3.MoveTowards(transform.position, Target, MoveSpeed * Time.deltaTime/ transform.localScale.x);


        //if (Input.GetAxis("Mouse X") < 0)
        //{
        //    this.transform.Translate(Vector3.left * MoveSpeed * Time.deltaTime);
        //}

        //if (Input.GetAxis("Mouse X") > 0)
        //{
        //    this.transform.Translate(Vector3.right * MoveSpeed * Time.deltaTime);
        //}

        //if (Input.GetAxis("Mouse Y") < 0)
        //{
        //    this.transform.Translate(Vector3.down * MoveSpeed * Time.deltaTime);
        //}

        //if (Input.GetAxis("Mouse X") > 0)
        //{
        //    this.transform.Translate(Vector3.up * MoveSpeed * Time.deltaTime);
        //}
    }
}
