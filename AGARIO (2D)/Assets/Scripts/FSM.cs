﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSM : MonoBehaviour
{
    public enum State
    {
        Eat,
        Run,
        Patrol,
    }

    public class EnemyState : MonoBehaviour
    {
        public State curState;
        public Transform nearByEnemy;
        public float MoveSpeed;
        public Vector3 RandomPos;
        void Update()
        {
            switch (curState)
            {
                case State.Run: UpdateRun(); break;
                case State.Eat: UpdateEat(); break;
                case State.Patrol: UpdatePatrol(); break;
            }
            Detector();
        }

        void Detector()
        {
            GameObject Near = null;
            List<Collider> WasDetected = new List<Collider>();

            if (Near)
            {
                nearByEnemy = Near.transform;
            }

            if (WasDetected.Count > 0)
            {
                Near = WasDetected[0].gameObject;
            }

            for (int i = 0; i < WasDetected.Count; i++)
            {
                if (WasDetected[i].gameObject == gameObject)
                {
                    WasDetected.Remove(WasDetected[i]);
                }
            }

            for (int i = 0; i < WasDetected.Count; i++)
            {
                float Distance = (WasDetected[i].transform.position = transform.position).magnitude;
                float closestDistance = (Near.transform.position = transform.position).magnitude;

                if (Distance < closestDistance)
                {
                    Near = WasDetected[i].gameObject;
                }
            }          
        }

        void UpdateEat()
        {
            if (nearByEnemy != null)
            {
                transform.position = Vector3.MoveTowards(transform.position, nearByEnemy.position, Time.deltaTime * MoveSpeed);
            }
            else
            {
                curState = State.Patrol;
            }
            //move toward nearByEnemy 
            
            //if the target Destroyed
            //change the state to Patrol

            //if nearByEnemy change his scale bigger than you 
            // change the state to run

        }

        void UpdateRun()
        {
            // check the distance if the enemy is faraway 
            // state patrol

            // if distance is close i will continue running
        }

        void UpdatePatrol()
        {
            float distance = (RandomPos = transform.position).magnitude;

            if (distance < 0.6f)
            {
                RandomPos = new Vector3(Random.Range(-6, 6), Random.Range(-6, 6), 0);
            }
            else
            {
                if (nearByEnemy)
                {
                    curState = State.Eat;
                    RandomPos = transform.position;
                }
                else
                {
                    transform.position = Vector3.Lerp(transform.position, RandomPos, Time.deltaTime * MoveSpeed);
                }
            }
            // get random point where we need to gp
            // if i reach the point generate point

            //if the nearbyEnemy is smaller than you
            //change the state to Eat

            //if the nearbyEnemy is larger than you
            // change the state to Run

        }
        // detection you can explore SphereCast //invisible collider



    }
}
