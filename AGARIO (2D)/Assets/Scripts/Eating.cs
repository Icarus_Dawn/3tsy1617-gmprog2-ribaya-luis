﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Eating : MonoBehaviour
{
    //FoodSpawn limit = new FoodSpawn();
    public Text Scoretext;
    public int Score;
    public float AddSize;
    public float sizeSpeed = 2;
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Food")
        {
            //limit.spawnlimit--;
            transform.localScale += new Vector3(AddSize, AddSize, AddSize);
            Camera.main.orthographicSize += 0.1f;
            Destroy(other.gameObject);

            Score += 10;
            Scoretext.text = "Score: " + Score;
        }

        if (other.gameObject.tag == "Poison")
        {
            transform.localScale -= new Vector3(AddSize, AddSize, AddSize);
            Camera.main.orthographicSize -= 0.1f;
            Destroy(other.gameObject);
        }

        if (other.gameObject.tag == "Enemy")
        {
            Destroy(this.gameObject);
            SceneManager.LoadScene("EndGame");
        }
    }
}
