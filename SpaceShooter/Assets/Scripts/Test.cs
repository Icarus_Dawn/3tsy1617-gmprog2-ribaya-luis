﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Test : MonoBehaviour
{
    public GameObject prefabBullet;
    float Speed = 10;
    public Text Score;
    public int currentscore = 0;
    public Text Life;
    public int currentlife = 3;
    // Use this for initialization
    void Start ()
    {
        
	}
	// Update is called once per frame
	void Update ()
    {
            
        //Debug.Log("Hello World");

        //if (Input.GetKey("up"))
        //{
        //    this.transform.Translate(Vector3.up * Speed * Time.smoothDeltaTime);
        //}

        //if (Input.GetKey("down"))
        //{
        //    this.transform.Translate(Vector3.down * Speed * Time.smoothDeltaTime);
        //}

        if (Input.GetKey("left"))
        {
            this.transform.Translate(Vector3.left * Speed * Time.smoothDeltaTime);
        }

        if (Input.GetKey("right"))
        {
            this.transform.Translate(Vector3.right * Speed * Time.smoothDeltaTime);
        }

        if (Input.GetKeyDown("space"))
        {
            GameObject objBullet;
            objBullet = (GameObject)Instantiate(prefabBullet, this.transform.position, Quaternion.identity);
            Destroy(objBullet, 2);
        }
        Life.text = "Lives: " + currentlife;
        Score.text = "Score: " + currentscore;
    }
}
