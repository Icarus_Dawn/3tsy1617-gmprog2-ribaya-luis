﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletScript : MonoBehaviour
{
    public float Bulletspeed;
    Test test;
	// Use this for initialization
	void Start ()
    {
	}

	// Update is called once per frame
	void Update ()
    {
        this.transform.Translate(Vector2.up * Bulletspeed * Time.deltaTime);

    }

    //void OnCollisionEnter(Collision other)
    //{

    //    Debug.Log("Hit! " + other.gameObject.name);
    //}

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Asteroid")
        {
            Destroy(other.gameObject);
            test.currentscore += 10;
        }
        //Debug.Log("Hit! " + other.gameObject.name);
    }
}
