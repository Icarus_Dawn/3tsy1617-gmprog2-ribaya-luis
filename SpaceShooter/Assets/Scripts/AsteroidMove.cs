﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AsteroidMove : MonoBehaviour
{
    public float AsteroidSpeed;
    public float RotateSpeed;
    public GameObject prefabAsteroid;
    Test _test;
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        this.transform.Translate(Vector2.down * AsteroidSpeed * Time.deltaTime);
        this.transform.Rotate(Vector3.down * RotateSpeed * Time.deltaTime);

	}
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            SceneManager.LoadScene("GameOverLose");
            _test.currentlife -= 1;
            
            if (_test.currentlife <= 0)
            {
                Destroy(other.gameObject);
                
            }
        }
        Debug.Log("Hit! " + other.gameObject.name);
    }
}
