﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawn : MonoBehaviour
{
    public GameObject PrefabAsteroid;
    float minmass = 0.2f;
    float maxmass = 0.4f;
    Vector3 Minspawnpoint;
    Vector3 Maxspawnpoint;
    public float Minspawninterval = 1.0f;
    public float Maxspawninterval = 3.0f;

    //Use this for initialization

   void Start()
    {
        StartCoroutine(SpawnTask());
    }

    IEnumerator SpawnTask()
    {
        while (true)
        {
            float waittime = Random.Range(Minspawninterval, Maxspawninterval);
            yield return new WaitForSeconds(waittime);
            SpawnAsteroid();
        }
    }

   // Update is called once per frame
    void SpawnAsteroid()
    {
        GameObject Asteroid = Instantiate(PrefabAsteroid);

        float x = Random.Range(Minspawnpoint.x, Maxspawnpoint.x);
        float y = Random.Range(Minspawnpoint.y, Maxspawnpoint.y);
        float z = Random.Range(Minspawnpoint.z, Maxspawnpoint.z);

        Asteroid.transform.position = new Vector3(x, y, z);

        Asteroid.GetComponent<Rigidbody>().mass = Random.Range(minmass, maxmass);
    }
}
